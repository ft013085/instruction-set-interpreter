#include <iostream>
#include <iomanip>
#include <string>
#include <sstream>
#include <fstream>
#include <vector>
#include <windows.h> // For Windows-specific console functions

using namespace std;

// Define colors
#define GREEN   "\033[32m"
#define BLUE    "\033[34m"
#define RESET   "\033[0m"

// Function to change console color
void setColor(const string& color) {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 15); // Reset color
    if (color == "green") {
        SetConsoleTextAttribute(hConsole, 10); // Green color
    }
    else if (color == "blue") {
        SetConsoleTextAttribute(hConsole, 9); // Blue color
    }
}

// Function to reset console color
void resetColor() {
    HANDLE hConsole = GetStdHandle(STD_OUTPUT_HANDLE);
    SetConsoleTextAttribute(hConsole, 15); // Reset color
}

// Define a structure for an instruction
struct Instruction {
    char opcode; // 1 character for opcode
    char regR;   // 1 character for register R
    char regS;   // 1 character for register S
    char regT;   // 1 character for register T
    string memoryAddress; // Memory address as a string
};

// Define structure for computer's registers and memory
struct Computer {
    vector<int> registers; // General purpose registers
    vector<int> memory;    // Memory cells
};

// Function to initialize the computer
void initializeComputer(Computer& computer) {
    computer.registers.resize(16); // 16 general purpose registers
    computer.memory.resize(256);   // 256 memory cells
}

// Function to decode and describe an instruction
string decodeInstruction(const Instruction& instr, const Computer& computer) {
    string description;

    // Decode opcode
    switch (instr.opcode) {
    case '1':
        description = "LOAD register ";
        description += instr.regR;
        description += " from memory cell ";
        description += instr.memoryAddress;
        break;
    case '2':
        description = "LOAD register ";
        description += instr.regR;
        description += " with value ";
        description += instr.memoryAddress;
        break;
    case '3':
        description = "STORE register ";
        description += instr.regR;
        description += " at memory cell ";
        description += instr.memoryAddress;
        break;
    case '4':
        description = "MOVE register ";
        description += instr.regR;
        description += " to register ";
        description += instr.regS;
        break;
    case '5':
        description = "ADD ";
        description += instr.regS;
        description += " and ";
        description += instr.regT;
        description += " into register ";
        description += instr.regR;
        break;
    case '6':
        description = "ADD (floating point) ";
        description += instr.regS;
        description += " and ";
        description += instr.regT;
        description += " into register ";
        description += instr.regR;
        break;
    case '7':
        description = "OR ";
        description += instr.regS;
        description += " and ";
        description += instr.regT;
        description += " into register ";
        description += instr.regR;
        break;
    case '8':
        description = "AND ";
        description += instr.regS;
        description += " and ";
        description += instr.regT;
        description += " into register ";
        description += instr.regR;
        break;
    case '9':
        description = "XOR ";
        description += instr.regS;
        description += " and ";
        description += instr.regT;
        description += " into register ";
        description += instr.regR;
        break;
    case 'A':
        description = "ROTATE register ";
        description += instr.regR;
        description += " right ";
        description += instr.memoryAddress;
        description += " times";
        break;
    case 'B':
        description = "JUMP to ";
        description += instr.memoryAddress;
        description += " if register ";
        description += instr.regR;
        description += " equals 0";
        break;
    case 'C':
        description = "HALT";
        break;
    default:
        description = "Invalid instruction";
        break;
    }

    return description;
}

// Function to convert a hexadecimal string to decimal
int hexToDecimal(const string& hex) {
    return stoi(hex, nullptr, 16);
}

// Function to parse an instruction from a hexadecimal string
Instruction parseInstruction(const string& hexString) {
    Instruction instr;
    instr.opcode = hexString[0];
    instr.regR = hexString[1];
    instr.regS = hexString[2];
    instr.regT = hexString[3];
    instr.memoryAddress = hexString.substr(2); // Memory address is the last two characters
    return instr;
}

// Function to read instructions from a file and interpret them
void interpretFromFile(const string& filename, Computer& computer) {
    ifstream file(filename);
    if (!file.is_open()) {
        cerr << "Error: Unable to open file '" << filename << "'." << endl;
        return;
    }

    string line;
    while (getline(file, line)) {
        if (line.length() != 4) {
            cerr << "Error: Invalid instruction format in file." << endl;
            continue;
        }

        // Parse the input string into an instruction
        Instruction instr = parseInstruction(line);

        // Decode and describe the instruction
        string description = decodeInstruction(instr, computer);
        cout << "Instruction: ";
        setColor("blue");
        cout << line;
        resetColor();
        cout << endl;
        cout << "Description: " << description << endl;
    }

    file.close();
}

int main() {
    char mode;
    cout << "Select mode: (L)ive interpreter or (F)ile mode: ";
    setColor("green");
    cin >> mode;
    resetColor();

    Computer computer;
    initializeComputer(computer); // Initialize the computer

    if (mode == 'L' || mode == 'l') {
        // Live interpreter mode
        string input;
        char choice;

        do {
            cout << "Enter hexadecimal instruction: ";
            cin >> input;

            // Validate input length
            if (input.length() != 4) {
                cerr << "Invalid input length. Please enter a 4-character hexadecimal string.\n";
                continue;
            }

            // Parse the input string into an instruction
            Instruction instr = parseInstruction(input);

            // Decode and describe the instruction
            string description = decodeInstruction(instr, computer);
            cout << "Instruction: " << input << endl;
            cout << "Description: " << description << endl;

            cout << "Do you want to enter another instruction? (Y/N): ";
            setColor("green");
            cin >> choice;
            resetColor();

        } while (choice == 'Y' || choice == 'y');
    }
    else if (mode == 'F' || mode == 'f') {
        // File mode
        interpretFromFile("instructions.txt", computer); // Interpret instructions from file
    }
    else {
        cout << "Invalid mode selected." << endl;
    }

    return 0;
}
